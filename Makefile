all: filesystem

filesystem:
	msbuild /t:Rebuild /p:Configuration=Release
	mkdir bin
	cp ./FileSystem/obj/x86/Release/FileSystem.exe ./bin/
	cp list.txt ./bin/

clean:
	rm -Rf ./bin/
	msbuild /t:Clean

run:
	cd ./bin/; mono FileSystem.exe
