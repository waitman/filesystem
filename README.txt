

FileSystem
Waitman Gobble <waitman@waitman.net> +1 829-914-3703
ISSUES/TODO
Doesn't copy / move / delete recursively.
Doesn't copy / move / delete folders.
Buttons seem to have to be double-clicked to function (GTK issue?).
Doesn't prompt before delete.
Doesn't move to trash, just deletes.


Needs mono c# and msbuild, and gtksharp libraries

to build, take a look at the Makefile. You can use the commands in the Makefile if you want,
or use them makefile like this:

make 

This will build the EXE, then create a directory in your current path "bin" and copy the EXE file and the list.txt file in the current directory to ./bin/

make clean

This will clean the build files, and also delete ./bin. (you will your list.txt. so probably should edit list.txt here and not in ./bin/)

make run

This will change to the ./bin directory and run the application.



DEBIAN:

apt-get install mono-xbuild
apt-get install gtk-sharp3




