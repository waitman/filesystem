﻿using System;
using System.IO;
using System.Diagnostics;
using Gtk;

/* FileSystem
 * Waitman Gobble <waitman@waitman.net> +1 829-914-3703
 * ISSUES/TODO
 * Doesn't copy / move / delete recursively.
 * Doesn't copy / move / delete folders.
 * Buttons seem to have to be double-clicked to function (GTK issue?).
 * Doesn't prompt before delete.
 * Doesn't move to trash, just deletes.
 */

public partial class MainWindow : Gtk.Window
{
    string bin_path = "/usr/local/bin/";
    string terminal = "/usr/local/bin/konsole";

    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();

        /* Add Combo Box Options to "Launch App" function
         * these are in the file list.txt, one per line
         * NOTE: Defaults to /usr/local/bin/
         * see: bin_path, above
         * On Linux you probably want to use /usr/bin/
         */

        combobox1.Clear();
        CellRendererText cell = new CellRendererText();
        combobox1.PackStart(cell, false);
        combobox1.AddAttribute(cell, "text", 0);
        ListStore store = new ListStore(typeof(string));
        combobox1.Model = store;
        var lines = File.ReadLines("list.txt");
        foreach (var line in lines)
        {
            store.AppendValues(line);
        }

    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnFileSelectionChangeEvent (object sender, EventArgs args)
    {
        /* When user changes the selected file, get some info */

        entry3.Text = "";
        entry4.Text = "";
        entry5.Text = "";
        entry7.Text = "";
        entry8.Text = "";
        image1.Pixbuf = null; //check this, might cause a memory leak.

        calendar2.Date = DateTime.Today;

        if (filechooserwidget2.Filename!=null)
        {
            entry3.Text = filechooserwidget2.Filename;

            try
            {
                FileInfo info = new FileInfo(filechooserwidget2.Filename);
                long length = info.Length;
                entry4.Text = length.ToString("n0") + "B = " + HumanReadable(length);
                calendar2.Date = info.LastWriteTime;
                entry7.Text = info.LastWriteTime.ToLongDateString() + " " + info.LastWriteTime.ToLongTimeString();

                DateTime now = DateTime.Now;
                TimeSpan elapsed = now.Subtract(info.LastWriteTime);
                double daysAgo = elapsed.TotalDays;
                String days = Math.Ceiling(daysAgo).ToString("n0");
                String ss = "";
                if (days!="1") 
                {
                    ss = "s";
                }
                entry8.Text = days + " day"+ss+" ago";

                var pixbuf = new Gdk.Pixbuf(filechooserwidget2.Filename);
                int pw = pixbuf.Width;
                int ph = pixbuf.Height;
                int npw = 240;
                int nph = 240;
                if (pw > ph)
                {
                    decimal scale = (decimal)ph / (decimal)pw;
                    npw = 240;
                    nph = (int)Math.Ceiling(npw * scale);
                }
                if (pw < ph)
                {
                    decimal scale = (decimal)pw / (decimal)ph;
                    nph = 240;
                    npw = (int)Math.Ceiling(nph * scale);
                }
                if ((npw > 0) && (nph > 0))
                {
                    entry5.Text = pw.ToString() + " x " + ph.ToString();
                    pixbuf = pixbuf.ScaleSimple(npw, nph, Gdk.InterpType.Bilinear);
                    image1.Pixbuf = pixbuf;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }

    static String HumanReadable(long ByteSize)
    {
        /* if you have an EB, let me know */
        string[] suffix = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
        if (ByteSize == 0)
        {
            return "0" + suffix[0];
        }
        long bytes = Math.Abs(ByteSize);
        int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
        double num = Math.Round(bytes / Math.Pow(1024, place), 1);
        return (Math.Sign(ByteSize) * num).ToString() + suffix[place];
    }

    protected void AppLauncher(object sender, ButtonPressEventArgs a)
    {
        if (entry3.Text != "")
        {
            var app = combobox1.Entry.Text;
            Console.WriteLine("App Launch: " + app + " " + entry3.Text);
            Process.Start(new ProcessStartInfo(
                bin_path + app,
                entry3.Text)
            { UseShellExecute = false });

        }

    }

    protected void LaunchTerminal(object sender, ButtonPressEventArgs a)
    {
        if (entry3.Text != "")
        {
            String path = System.IO.Path.GetDirectoryName(entry3.Text);
            Console.WriteLine(path);
            var startInfo = new ProcessStartInfo(terminal);
            startInfo.WorkingDirectory = path;
            startInfo.UseShellExecute = true;
            Process.Start(startInfo);
            
        } else {
            var startInfo = new ProcessStartInfo(terminal);
            startInfo.UseShellExecute = false;
            Process.Start(startInfo);
        }
    }

    protected void CopyFiles(object send, ButtonPressEventArgs args)
    {
        if (filechooserwidget3.Filename != null)
        {
            string dest = filechooserwidget3.CurrentFolder;
            String[] src = filechooserwidget2.Filenames;
            int steps = src.Length + 1;
            int step = 0;
            foreach (String srcfile in src)
            {
                try
                {
                    String srcfn = System.IO.Path.GetFileName(srcfile);
                    File.Copy(srcfile, System.IO.Path.Combine(dest, srcfn));
                    step++;
                    progressbar1.Fraction = (double)(step/steps);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            progressbar1.Fraction = 0;
        }
    }

    protected void DeleteFiles(object send, ButtonPressEventArgs args)
    {
        String[] src = filechooserwidget2.Filenames;
        if (src.Length>0)
        {
            int steps = src.Length + 1;
            int step = 0;
            foreach (String srcfile in src)
            {
                try
                {
                    File.Delete(srcfile);
                    step++;
                    progressbar1.Fraction = (double)(step / steps);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            progressbar1.Fraction = 0;
        }
    }

    protected void MoveFiles(object send, ButtonPressEventArgs args)
    {
        if (filechooserwidget3.Filename != null)
        {
            string dest = filechooserwidget3.CurrentFolder;
            String[] src = filechooserwidget2.Filenames;
            int steps = src.Length + 1;
            int step = 0;
            foreach (String srcfile in src)
            {
                try
                {
                    String srcfn = System.IO.Path.GetFileName(srcfile);
                    File.Move(srcfile, System.IO.Path.Combine(dest, srcfn));
                    step++;
                    progressbar1.Fraction = (double)(step / steps);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            progressbar1.Fraction = 0;
        }
    }

    protected void ShowDestPath(object sender, EventArgs args)
    {
        if (filechooserwidget3.Filename != null)
        {
            entry9.Text = filechooserwidget3.CurrentFolder;
        }
    }
}
